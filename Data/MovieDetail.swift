//
//  MovieDetail.swift
//  TheMovieApp
//
//  Created by Augustine Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import Foundation

protocol PropertyNames {
    func propertyNames() -> [String]
}

extension PropertyNames
{
    func propertyNames() -> [String] {
        return Mirror(reflecting: self).children.compactMap { $0.label }
    }
}

class MovieDetail: PropertyNames, Decodable {
    
    let adult: Bool?
    let backdrop_path: String?
    let budget: Int?
    let id: Int?
    let imdb_id: String?
    let original_language: String?
    let original_title: String?
    let overview: String?
    let popularity: Float?
    let poster_path: String?
    let production_countries: [JSON<String>]?
    let release_date: String?
    let revenue: Int?
    let runtime: Int?
    let spoken_languages: [JSON<String>]?
    let status: String?
    let tagline: String?
    let title: String?
    let video: Bool?
    let vote_average: Float?
    let vote_count: Int?
}

