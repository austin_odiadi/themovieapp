//
//  DataManager.swift
//  TheMovieApp
//
//  Created by Augustine Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import Foundation
import PromiseKit

typealias Json = [String : String]?
typealias JSON<T> = Dictionary<String, T>

protocol MoviesManager {
    func sort(movies: [Movie]?) -> Promise<[Movie]>
}

protocol MovieDetailManager {
    
}


class DataManager { }
    


extension DataManager: MoviesManager {
    func sort(movies: [Movie]?) -> Promise<[Movie]> {
        
        return Promise<[Movie]> { seal in
            if let sorted = movies?.sorted(by: {$0.title!.compare($1.title!) == .orderedDescending}) {
                seal.resolve(.fulfilled(sorted))
            }
            else {
                seal.resolve(.fulfilled([]))
            }
        }
    }
}


extension DataManager: MovieDetailManager {
    
}
