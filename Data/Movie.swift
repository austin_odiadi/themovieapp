//
//  Movie.swift
//  TheMovieApp
//
//  Created by Augustine Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import Foundation

class Movie: Decodable {
    let popularity: Float?
    let vote_count: Float?
    let video: Bool?
    let poster_path: String?
    let id: Int?
    let adult: Bool?
    let backdrop_path: String?
    let original_language: String?
    let original_title: String?
    let genre_ids: [Int]?
    let title: String?
    let vote_average: Float?
    let overview: String?
    let release_date: String?
}
