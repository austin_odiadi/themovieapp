//
//  Downloader.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 10/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit


//typealias Poster = Data

protocol DownloaderProtocol {
    func download<T, U>(arg: T?) -> Promise<U>
}

class Downloader <T: Decodable> {
    
    var api: APICommons<T>? = nil
    init() {
        self.api = APICommons<T>()
    }
}


