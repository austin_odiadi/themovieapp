//
//  ImageDownloader.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 14/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import Foundation
import PromiseKit

class ImageDownloader<T: Decodable>: Downloader<T> {
    override init() {
    }
}

extension ImageDownloader: DownloaderProtocol {
    
    func download<T, U>(arg: T?) -> Promise<U> {
        return api?.download(arg, type: .data) as! Promise<U>
    }
}

extension ImageDownloader {
    func image(for name: String) -> Promise<UIImage> {
        return Promise<UIImage> { seal in
            download(arg: name).done {
                seal.resolve(.fulfilled(UIImage(data: $0)!))
            }.cauterize()
        }
    }
}
