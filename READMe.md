This is a movie App that gets a list of top movies and renders them in a list view. On selection further details for the selection is presented.

Third Party libraries used
=

PromiseKit

UI Arch
=
VIPER: View, Interactor, Presenter, Entity & Router

DI Framework
=

IoC Container

Other
=

Downloads are done using standard iOS frameworks and APIs

Storybord with minimal constraints used

TableView Cells with minimal constraints

No Database used, but CoreData would have been the ideal choice

The App does not support dark mode

Testing not implemented due to time constraint

Pseudo for testing

```
    SKContainer()
                    .register(<Inject a manually instantiated API call with predefined data>)
                    .register(<Inject a manually instantiated API call with predefined data>)
                    .register(<Inject a manually instantiated API call with predefined data>)
                    .register(MoviesViewController.self) {
                        let interactor = MoviesInteractor(movieAPI: $0.resolve(APICommons<Movie>.self))
                        
                        let viewController = MoviesRouter.createModule(interactor: interactor)
                        viewController.detailsVCFactory = $0.factory(for: MovieDetailViewController.self)
                        return viewController
                    }
                    .register(MovieDetailViewController.self) {
                        let interactor = MovieDetailInteractor(detailAPI: $0.resolve(APICommons<MovieDetail>.self),
                                                               posterAPI: $0.resolve(ImageDownloader<Poster>.self))
                        
                        let viewController = MovieDetailRouter.createModule(interactor: interactor)
                        return viewController
                    }
    }

    // Load VC
    window?.rootViewController = UINavigationController(rootViewController: container.resolve(MoviesViewController.self))
```
