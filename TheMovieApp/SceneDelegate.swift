//
//  SceneDelegate.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        guard let windowScene = (scene as? UIWindowScene) else { return }
        
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        
        let container = SKContainer.configure()
        let viewController = container.resolve(MoviesViewController.self)

        window?.rootViewController = UINavigationController(rootViewController: viewController)
        window?.makeKeyAndVisible()
        
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
    }
}

extension SKContainer {
    static func configure() -> SKContainer {
        return  SKContainer()
                    .register(APICommons<Movie>.self, APICommons<Movie>())
                    .register(APICommons<MovieDetail>.self, APICommons<MovieDetail>())
                    .register(ImageDownloader<Data>.self, ImageDownloader())
                    .register(MoviesViewController.self) {
                        let interactor = MoviesInteractor(movieAPI: $0.resolve(APICommons<Movie>.self))
                        
                        let viewController = MoviesRouter.createModule(interactor: interactor)
                        viewController.detailsVCFactory = $0.factory(for: MovieDetailViewController.self)
                        return viewController
                    }
                    .register(MovieDetailViewController.self) {
                        let interactor = MovieDetailInteractor(detailAPI: $0.resolve(APICommons<MovieDetail>.self),
                                                               posterAPI: $0.resolve(ImageDownloader<Data>.self))
                        
                        let viewController = MovieDetailRouter.createModule(interactor: interactor)
                        return viewController
                    }
    }
}

