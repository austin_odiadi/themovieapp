//
//  Indirect.swift
//  IoCTestInterface
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Augustine Odiadi. All rights reserved.
//

import Foundation

struct Indirect<T> {
    
    // Class wrapper to provide the actual indirection.
    private final class Wrapper {
        
        var value: T
        
        init(_ value: T) {
            self.value = value
        }
    }
    
    private var wrapper: Wrapper
    
    init(_ value: T) {
        wrapper = Wrapper(value)
    }
    
    var value: T {
        get {
            return wrapper.value
        }
        set {
            if isKnownUniquelyReferenced(&wrapper) {
                wrapper.value = newValue
            } else {
                wrapper = Wrapper(newValue)
            }
        }
    }
}
