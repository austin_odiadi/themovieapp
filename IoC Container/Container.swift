//
//  Container.swift
//  IoC
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Augustine Odiadi. All rights reserved.
//

import Foundation


public struct SKContainer: Assembler {
    
    var factories: [Indirect<ServiceFactory>]
    var resolvedInstance: ResolvedInstances
    
    public init() {
        self.factories = []
        self.resolvedInstance = ResolvedInstances()
    }
    
    private init(factories: [Indirect<ServiceFactory>], resolved: ResolvedInstances) {
        self.factories = factories
        self.resolvedInstance = resolved
    }
}

extension SKContainer: Resolver {
    
    public func resolve<T>(_ type: T.Type) -> T {
        
        guard let factory = _factory(type) else {
            fatalError()
        }

        // Initial logic attempt to resolve circular dependecies
        // The logic has not been completely thought of but the basics
        // includes storing resolved instances and based on type and propertes
        // and return an already instantiation of a property instance rather
        // than initializing a new instance.
        
        // Please this is a theoretical approach and has not bee fully
        // implemented so could be some flaws to that plan.
        if let instance: T = resolvedInstance.instance(type) as? T {
            return instance
        }
        else if resolvedInstance.type(type) {
            print()
        }

        let instance: T = factory.resolve(self)
        resolvedInstance.storeInstance(instance: instance)

        return instance
    }
}

extension SKContainer: Register {
    
    public func register<T>(_ interface: T.Type, _ instance: T) -> SKContainer {
        return register(interface) { _ in instance }
    }

    public func register<T>(_ type: T.Type, _ factory: @escaping (Resolver) -> T) -> SKContainer {
        assert(!_contains(type))

        let service = SKService<T>(type, factory: {
            factory($0)
        })
    
        return .init(factories: factories + [Indirect<ServiceFactory>(ServiceFactory(service))], resolved: resolvedInstance)
    }
}

extension SKContainer: Factory {
    
    public func factory<ServiceType>(for type: ServiceType.Type) -> () -> ServiceType {
        guard let factory = _factory(type) else {
            fatalError()
        }

        return { factory.resolve(self) }
    }
}
