//
//  Register.swift
//  IoC
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Augustine Odiadi. All rights reserved.
//

import Foundation

public protocol Register {
    
    func register<T>(_ interface: T.Type, _ instance: T) -> Self
    func register<T>(_ type: T.Type, _ factory: @escaping (Resolver) -> T) -> Self
}

