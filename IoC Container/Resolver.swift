//
//  Resolver.swift
//  IoC
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Augustine Odiadi. All rights reserved.
//

import Foundation

public protocol Resolver {
    func resolve<T>(_ type: T.Type) -> T
}

extension Resolver {
    public func factory<T>(for type: T.Type) -> () -> T {
        return { self.resolve(type) }
    }
}
