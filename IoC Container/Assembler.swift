//
//  Assembler.swift
//  IoC
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Augustine Odiadi. All rights reserved.
//

import Foundation

protocol Assembler: Resolver, Register, Factory {
    var factories: [Indirect<ServiceFactory>] { set get }
}

extension Assembler {
    public func _factory<T>(_ type: T.Type) -> ServiceFactory? {
        guard let factory = factories.first(where: { $0.value.contains(type) }) else {
             fatalError(SKError.factoryNotFound(type: T.self).description)
         }
         
        return factory.value
     }
    
    func _contains<T>(_ type: T.Type) -> Bool {
        return factories.contains(where: { $0.value.contains(type) })
    }
}
