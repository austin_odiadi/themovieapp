//
//  Service.swift
//  IoC
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Augustine Odiadi. All rights reserved.
//

import Foundation

protocol Service {
    associatedtype ServiceType

    func resolve(_ resolver: Resolver) -> ServiceType
}

protocol FactoryProtocol: class {
    var service: Any { set get }
    var supportType: Any.Type { set get }
}

extension FactoryProtocol where Self: ServiceFactory {
    
    private func service<T>(_ resolver: Resolver) -> SKService<T>? {
        guard let service = self.service as? SKService<T> else {
           fatalError(SKError.serviceNotFound(type: T.self).description)
        }
        
        return service
    }
    
    func _resolve<T>(_ resolver: Resolver) -> T {
        guard let service: SKService<T> = service(resolver) else {
            fatalError()
        }
        
        return service.resolve(resolver)
    }

    func _contains<T>(_ type: T.Type) -> Bool {
        return self.supportType == type
    }
}

final class ServiceFactory: FactoryProtocol {
    
    var service: Any
    var supportType: Any.Type

    init<T: Service>(_ service: T) {
        self.service = service
        self.supportType = T.ServiceType.self
    }

    func resolve<T>(_ resolver: Resolver) -> T {
        return _resolve(resolver)
    }
    
    func contains<T>(_ type: T.Type) -> Bool {
        return _contains(type)
    }
}

struct SKService<T>: Service { 
    private let factory: (Resolver) -> T
    
    init(_ type: ServiceType.Type, factory: @escaping (Resolver) -> T) {
        self.factory = factory
    }

    func resolve(_ resolver: Resolver) -> T {
        return factory(resolver)
    }
}

extension Service {
    func contains<T>(_ type: T.Type) -> Bool {
        return type == ServiceType.self
    }
}
