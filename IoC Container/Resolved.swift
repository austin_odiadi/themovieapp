//
//  Resolved.swift
//  IoCTestInterface
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Augustine Odiadi. All rights reserved.
//

import Foundation

public class ResolvedInstances {
    internal var resolvedInstances = [String : Any]()
    internal var resolvableInstances = [Resolved]()
    
    init() {
    }
    
    func storeInstance<T>(instance: T) {
        resolvedInstances[T.Type.self] = instance
    }
    
    func instance<T>(_ type: T) -> T? {
        return resolvedInstances[T] as? T
    }

    func type<T>(_ type: T.Type) -> Bool {
        return resolvableInstances.contains(where: { $0.contains(type) })
    }
    
    func storeResolvable<T>(_ type: T.Type) {
        resolvableInstances.append(Resolved(type: type))
    }
}

public class Resolved {
    var type: Any.Type
    
    init<T>(type: T.Type) {
        self.type = type
    }
    
    func contains<T>(_ type: T.Type) -> Bool {
        return self.type == type
    }
}

extension Resolved {
    
}

extension Dictionary where Key : LosslessStringConvertible {
    
    subscript(index: Any.Type) -> Value? {
        get {
            return self[String(describing: index) as! Key]
        }
        set(newValue) {
            self[String(describing: index) as! Key] = newValue
        }
   }
}

