//
//  Error.swift
//  IoC
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Augustine Odiadi. All rights reserved.
//

import UIKit
import Foundation

public enum SKError: Error, CustomStringConvertible {
    
    case serviceNotFound(type: Any.Type)
    case factoryNotFound(type: Any.Type)
    case invalidType(resolved: Any?)
    
    public var description: String {
        switch self {
        case let .invalidType(resolved):
          return "Unknown type \(resolved ?? "")."
        case let .factoryNotFound(type):
            return "No suitable factory found for type \(type)"
        case let .serviceNotFound(type):
            return "Service not found for type \(type)"
        }
    }
    
}
