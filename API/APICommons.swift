//
//  APICommons.swift
//  IoC
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Augustine Odiadi. All rights reserved.
//

import Foundation
import PromiseKit

struct DataList<T: Decodable>: Decodable {
    let results: [T]
}

class DataAction<T: Decodable>: APIClient {
    var session: URLSession
    
    init(configuration: URLSessionConfiguration? = .default) {
        self.session = URLSession(configuration: configuration!)
    }
    
    func getAll() -> Promise<DataList<T>> {
        fatalError()
    }
    
    func details(for id: Int) -> Promise<T> {
        fatalError()
    }
    
    func download<U>(_ data: U?, type: APIType) -> Promise<T> {
        fatalError()
    }
}

class APICommons<T: Decodable> : DataAction<T> {
    
    override func getAll() -> Promise<DataList<T>> {
        return makeRequest(request: Feed.type(T.self).request)
    }
    
    override func details(for id: Int) -> Promise<T> {
        return makeRequest(request: Feed.type(T.self).request("\(id)"))
    }
    
    override func download<U>(_ data: U?, type: APIType) -> Promise<T> {
        return makeRequest(request: Feed.type(T.self).request(data), type: type)
    }

    private func makeRequest<U: Decodable>(request: URLRequest, type: APIType? = .json) -> Promise<U> {
        return Promise<U> { seal in
        
            fetch(with: request, type: type!)
                .done { (arg: (Response<U, APIError>)) in
                    switch arg {
                    case .success(let data):
                        seal.fulfill(data)
                    case .failure(let error):
                        seal.resolve(.rejected(error))
                    }
                }
                .catch { error in
                    seal.resolve(.rejected(error))
                }
        }
    }
}
