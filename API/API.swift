//
//  API.swift
//  IoC
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Augustine Odiadi. All rights reserved.
//

import Foundation
import PromiseKit

enum Response<T, U> {
    case success(T)
    case failure(U)
}

protocol URI {
    var baseURL: String { get }
    var imageURL: String { get }
    var endpoint: String { get }
}

extension URI {
    
    var apiKey: String {
        return "api_key=693bd17bc5edb6e717cacd0d5f786b43"
    }
    
    var urlComponents: URLComponents {
        var components = URLComponents(string: baseURL)!
        components.path = endpoint
        components.query = apiKey
        return components
    }
    
    var request: URLRequest {
        return URLRequest(url: urlComponents.url!)
    }
    
    func request<T>(_ arg: T ... ) -> URLRequest {
        
        var components = URLComponents(string: baseURL)!
        components.path = endpoint
        
        arg.forEach {
            components.path = String(format: components.path, $0 as! String)
        }
        
        components.query = apiKey
        
        return URLRequest(url: components.url!)
    }
}

enum Feed {
    case none
    case movies
    case poster
    case movieDetail
}

extension Feed {
    static func type<T>(_ type: T) -> Feed {
        switch type {
        case is Movie.Type: return .movies
        case is Data.Type: return .poster
        case is MovieDetail.Type: return .movieDetail
        default: return .none
        }
    }
}

extension Feed: URI {
    var imageURL: String {
        return "http://image.tmdb.org"
    }

    var baseURL: String {
        switch self  {
        case .poster: return "http://image.tmdb.org"
        default: return "https://api.themoviedb.org"
        }
    }
    
    var endpoint: String {
        switch self  {
        case .none: return ""
        case .poster: return "/t/p/w780/%@"
        case .movies: return "/3/movie/top_rated"
        case .movieDetail: return "/3/movie/%@"
        }
    }
}

enum APIError: Error {
    case failed
    case invalid
    
    var localizedDescription: String {
        switch self {
        case .failed: return "Request failed"
        case .invalid: return "Invalid data"
        }
    }
}

enum APIType {
    case json
    case data
}

protocol APIClient {
    var session: URLSession { get }
    func fetch<T: Decodable>(with request: URLRequest, type: APIType) -> Promise<Response<T, APIError>>
}

extension APIClient {
    
    func fetch<T: Decodable>(with request: URLRequest, type: APIType) -> Promise<Response<T, APIError>> {
        return Promise<Response<T, APIError>> { seal in
            
            _ = session.dataTask(with: request) { data, response, error in

                guard let httpResponse = response as? HTTPURLResponse else {
                    seal.resolve(nil, Response.failure(.failed))
                    return
                }
                
                switch httpResponse.statusCode {
                case 200..<299:
                    if let data = data {
                        do {
                            switch type {
                            case .json:
                                let json = try JSONDecoder().decode(T.self, from: data)
                                seal.resolve(.success(json), nil)
                                break
                            case .data:
                                seal.resolve(.success(data as! T), nil)
                                break
                            }
                        }
                        catch {
                            seal.resolve(nil, .failure(.invalid))
                        }
                    }
                    else {
                        seal.resolve(nil, .failure(.invalid))
                    }
                    break
                default :
                    seal.resolve(nil, .failure(.failed))
                    break
                }
            }.resume()
        }
    }
}
