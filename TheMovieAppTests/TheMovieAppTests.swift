//
//  TheMovieAppTests.swift
//  TheMovieAppTests
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import XCTest
@testable import TheMovieApp

class TheMovieAppTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testHelloWorld() {
        //self.expectation(description: "")
        var hello: String?
        
        XCTAssertNil(hello)
        
        hello = "Hellooo"
        
        XCTAssertEqual("Hellooo", hello)
    }

}
