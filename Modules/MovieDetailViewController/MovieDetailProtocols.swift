//
//  MovieDetailProtocols.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import Foundation
import PromiseKit

// MARK: WireFrameProtocol

protocol MovieDetailWireframeProtocol: class {

}

// MARK: PresenterProtocol

protocol MovieDetailPresenterProtocol: class {

    var interactor: MovieDetailInteractorInputProtocol? { get set }

    func initialize() -> Void
    func getMovieDetail(id: Int) -> Promise<MovieDetail>
    func getMoviePoster(named: String) -> Promise<UIImage>
}

// MARK: InteractorProtocol

protocol MovieDetailInteractorOutputProtocol: class {

    /** Interactor -> Presenter */
}

protocol MovieDetailInteractorInputProtocol: class {

    var presenter: MovieDetailInteractorOutputProtocol? { get set }
    var detailAPI: APICommons<MovieDetail>? { get set }
    var posterAPI: ImageDownloader<Data>? { get set }

    func movieDetail(id: Int) -> Promise<MovieDetail>
    func moviePoster(named: String) -> Promise<UIImage>
    /** Presenter -> Interactor */
}

// MARK: ViewProtocol

protocol MovieDetailViewProtocol: class {

    var presenter: MovieDetailPresenterProtocol? { get set }

    func initialized() -> Void
    /** Presenter -> ViewController */
}
