//
//  MovieDetailViewController.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import UIKit

class MovieDetailViewController: BaseViewController {

    var movie: Movie?
    var posterView: PosterView?
    var currentYPosition: CGPoint = .zero
    var reuseIdentifier = "MovieDetailCellIdentifier"
    
    @IBOutlet weak var tableView: UITableView!
    
    var properties: [String]?
    var movieDetail: MovieDetail? {
        didSet {
            tableView.reloadData()
        }
    }
    var presenter: MovieDetailPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.initialize()
    }
}

extension MovieDetailViewController: MovieDetailViewProtocol {
    
    func initialized() {
        guard let movieId = movie?.id else {
            return
        }
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets(top: 300, left: 0, bottom: 0, right: 0)
        tableView.register(UINib(nibName: "MovieDetailCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        
        posterView = PosterView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 300))
        posterView?.title.text = movie?.title
        view.addSubview(posterView!)
        
        DispatchQueue.global(qos: .utility).async {
            self.presenter?.getMoviePoster(named: (self.movie?.poster_path)!)
                .done { image in
                    DispatchQueue.main.async {
                        self.posterView?.poster.image = image
                    }
                }
                .cauterize()
        }
        
        presenter?.getMovieDetail(id: movieId)
            .done { detail in
                self.properties = detail.propertyNames()
                self.movieDetail = detail
            }
            .cauterize()
    }
}

extension MovieDetailViewController: UITableViewDelegate, UITableViewDataSource {

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.properties?.count {
            return count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! MovieDetailCell
        
        cell.title.text = properties?[indexPath.row].capitalized
        
        if let title = properties?[indexPath.row] {
            cell.configure(title:title, subtitle: subtitle(for: title)!)
        }
        
        return cell
    }
    
    func subtitle(for title: String) -> String? {
        switch title {
        case "title": return movieDetail?[keyPath: \MovieDetail.title]
        case "overview": return movieDetail?[keyPath: \MovieDetail.overview]
        case "adult": return movieDetail?[keyPath: \MovieDetail.adult]?.description
        case "budget": return "\(movieDetail?[keyPath: \MovieDetail.budget] ?? 0)"
        case "original_language": return movieDetail?[keyPath: \MovieDetail.original_language]
        case "original_title": return movieDetail?[keyPath: \MovieDetail.original_title]
        case "popularity": return "\(movieDetail?[keyPath: \MovieDetail.popularity] ?? 0)"
        default: break
        }
        
        return "N/A"
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = 300 - (scrollView.contentOffset.y + 300)
        let height = min(max(y, 60), 400)
        posterView?.update(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: height),
                           point: CGPoint(x: scrollView.contentOffset.x, y: scrollView.contentOffset.y - currentYPosition.y))
        
        currentYPosition = scrollView.contentOffset
    }
}
