//
//  PosterView.swift
//  TheMovieApp
//
//  Created by Augustine Odiadi on 11/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import UIKit

class PosterView: BaseUIView {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var poster: UIImageView!
    @IBOutlet weak var titleContainerBottom: NSLayoutConstraint!
    
    @IBOutlet weak var titleContainerHeight: NSLayoutConstraint!
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func initialize() {
        super.initialize()
        
        poster.contentMode = .scaleAspectFill
        poster.clipsToBounds = true
    }
    
    func update(frame: CGRect, point: CGPoint) {
        
        titleContainerHeight.constant = titleContainerHeight.constant + (point.y / 60)
        self.frame = frame
        
    }
}

