//
//  MovieDetailInteractor.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import Foundation
import PromiseKit

class MovieDetailInteractor: MovieDetailInteractorInputProtocol {

    var posterAPI: ImageDownloader<Data>?
    var detailAPI: APICommons<MovieDetail>?
    weak var presenter: MovieDetailInteractorOutputProtocol?

    init(detailAPI: APICommons<MovieDetail>, posterAPI: ImageDownloader<Data>) {
        self.detailAPI = detailAPI
        self.posterAPI = posterAPI
    }
    
    func movieDetail(id: Int) -> Promise<MovieDetail> {
        return Promise<MovieDetail> { seal in
            detailAPI?.details(for: id)
                .done { detail in
                    seal.resolve(.fulfilled(detail))
                }.cauterize()
        }
    }
    
    func moviePoster(named: String) -> Promise<UIImage> {
        return Promise<UIImage> { seal in
            posterAPI?.image(for: named).done {
                seal.resolve(.fulfilled($0))
            }.cauterize()
        }
    }
}
