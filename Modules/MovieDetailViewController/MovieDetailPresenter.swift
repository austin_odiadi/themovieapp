//
//  MovieDetailPresenter.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import UIKit
import PromiseKit

class MovieDetailPresenter: MovieDetailPresenterProtocol {
    
    weak private var view: MovieDetailViewProtocol?
    var interactor: MovieDetailInteractorInputProtocol?
    private let router: MovieDetailWireframeProtocol

    init(interface: MovieDetailViewProtocol, interactor: MovieDetailInteractorInputProtocol?, router: MovieDetailWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    func initialize() {
        // Run any intialization required
        
        view?.initialized()
    }
    
    func getMovieDetail(id: Int) -> Promise<MovieDetail> {
        return (interactor?.movieDetail(id: id))!
    }
    
    func getMoviePoster(named: String) -> Promise<UIImage> {
        return (interactor?.moviePoster(named: named))!
    }
}

extension MovieDetailPresenter: MovieDetailInteractorOutputProtocol {
    // MARK: MovieDetailInteractorOutputProtocol functions
}
