//
//  MovieDetailRouter.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import UIKit

class MovieDetailRouter: MovieDetailWireframeProtocol {

    weak var viewController: MovieDetailViewController?

    static func createModule(interactor: MovieDetailInteractor) -> MovieDetailViewController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let view: MovieDetailViewController = storyboard.instantiateViewController(identifier: "MovieDetailViewController")

        let router = MovieDetailRouter()
        let presenter = MovieDetailPresenter(interface: view, interactor: interactor, router: router)

        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view

        return view
    }

}
