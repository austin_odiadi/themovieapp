//
//  MovieDetailCell.swift
//  TheMovieApp
//
//  Created by Augustine Odiadi on 11/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import UIKit

class MovieDetailCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitile: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.title.text = nil
    }
    
    func configure(title: String, subtitle: String) {
        self.title.text = title.replacingOccurrences(of: "_", with: " ").uppercased()
        self.subTitile.text = subtitle + "\n"
    }
}
