//
//  BaseViewController.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 10/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}


extension UIViewController {
    func navTitle(_ title: String) {
        let titleLbl = UILabel()
        titleLbl.text = title
        titleLbl.textColor = UIColor.black
        titleLbl.font = UIFont.systemFont(ofSize: 20.0, weight: .bold)
        let titleView = UIStackView(arrangedSubviews: [titleLbl])
        titleView.axis = .horizontal
        navigationItem.titleView = titleView
    }
}
