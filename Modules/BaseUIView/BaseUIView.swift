//
//  BaseUIView.swift
//  TheMovieApp
//
//  Created by Augustine Odiadi on 11/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import UIKit
import Foundation

class BaseUIView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func initialize() {
        self.loadNib()
    }

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
    }
}


public extension UIView {

    @discardableResult
    func loadNib<T : UIView>() -> T? {
        guard let view = Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?[0] as? T else {
            return nil
        }
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat:"H:|[view]|", options: [], metrics:nil, views:["view":view]))
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat:"V:|[view]|", options: [], metrics:nil, views:["view":view]))
        return view
    }
}
