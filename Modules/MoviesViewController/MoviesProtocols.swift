//
//  MoviesProtocols.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import Foundation
import PromiseKit

// MARK: WireFrameProtocol

protocol MoviesWireframeProtocol: class {
    
}

// MARK: PresenterProtocol

protocol MoviesPresenterProtocol: class {

    var interactor: MoviesInteractorInputProtocol? { get set }

    func initialize() -> Void
    func getMovies() -> Promise<[Movie]>
}

// MARK: InteractorProtocol

protocol MoviesInteractorOutputProtocol: class {

    /** Interactor -> Presenter */
}

protocol MoviesInteractorInputProtocol: class {

    var presenter: MoviesInteractorOutputProtocol? { get set }
    var movieAPI: APICommons<Movie>? { get set }

    func movies() -> Promise<[Movie]>
    /** Presenter -> Interactor */
}

// MARK: ViewProtocol

protocol MoviesViewProtocol: class {

    var presenter: MoviesPresenterProtocol? { get set }
    
    func initialized() -> Void
    /** Presenter -> ViewController */
}
