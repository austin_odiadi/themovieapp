//
//  MoviesRouter.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
// Tested

import UIKit

class MoviesRouter: MoviesWireframeProtocol {

    weak var viewController: MoviesViewController?

    static func createModule(interactor: MoviesInteractor) -> MoviesViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let view: MoviesViewController = storyboard.instantiateViewController(identifier: "MoviesViewController")
        
        let router = MoviesRouter()
        let presenter = MoviesPresenter(interface: view, interactor: interactor, router: router)

        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view

        return view
    }

}
