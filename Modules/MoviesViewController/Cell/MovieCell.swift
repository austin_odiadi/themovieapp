//
//  MovieCell.swift
//  TheMovieApp
//
//  Created by Augustine Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import UIKit

class MovieCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var rating: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(movie: Movie?) {
        self.title?.text = movie?.title
        self.subTitle.text = "Released: \(movie?.release_date ?? "")"
        
        if let rating = movie?.vote_average {
            self.rating.text = "\(rating)"
        }
        
        self.selectionStyle = .none
    }
}
