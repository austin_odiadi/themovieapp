//
//  MoviesPresenter.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import UIKit
import PromiseKit

class MoviesPresenter: MoviesPresenterProtocol {
   
    
    

    weak private var view: MoviesViewProtocol?
    var interactor: MoviesInteractorInputProtocol?
    private let router: MoviesWireframeProtocol

    init(interface: MoviesViewProtocol, interactor: MoviesInteractorInputProtocol?, router: MoviesWireframeProtocol) {
        self.view = interface
        self.interactor = interactor
        self.router = router
    }

    func getMovies() -> Promise<[Movie]> {
        return (interactor?.movies())!
    }
    
    func initialize() {
        // Run any intialization required
        
        view?.initialized()
    }
}

extension MoviesPresenter: MoviesInteractorOutputProtocol {
    // MARK: MoviesInteractorOutputProtocol functions
}
