//
//  MoviesInteractor.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import Foundation
import PromiseKit

class MoviesInteractor: MoviesInteractorInputProtocol {
    
    
    var dataManager: DataManager?
    var movieAPI: APICommons<Movie>?
    weak var presenter: MoviesInteractorOutputProtocol?

    init(movieAPI: APICommons<Movie>) {
        self.movieAPI = movieAPI
        self.dataManager = DataManager()
    }
    
    func movies() -> Promise<[Movie]> {
        
        return Promise<[Movie]> { seal in
            movieAPI?.getAll()
                .then { movies in
                    return (self.dataManager?.sort(movies: movies.results))!
                }
                .done { sorted in
                    seal.resolve(.fulfilled(sorted))
                }.cauterize()
        }
        
    }
}
