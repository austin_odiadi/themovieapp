//
//  MoviesViewController.swift
//  TheMovieApp
//
//  Created by Austin Odiadi on 08/11/2019.
//  Copyright © 2019 Austin Odiadi. All rights reserved.
//

import UIKit

class MoviesViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var reuseIdentifier = "MovieCellIdentifier"
    
    var presenter: MoviesPresenterProtocol?
    var detailsVCFactory: () -> MovieDetailViewController = { fatalError() }
    
    var movies: [Movie]? {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.initialize()
        
    }
    
    func showDetails(movie: Movie) {

        let detailViewController = detailsVCFactory()
        detailViewController.movie = movie
        
        self.present(detailViewController, animated: true, completion: nil)
    }
}

extension MoviesViewController: MoviesViewProtocol {
    
    func initialized() {
        
        navTitle("Movies")
        self.tableView.register(UINib(nibName: "MovieCell", bundle: nil), forCellReuseIdentifier: reuseIdentifier)
        
        presenter?.getMovies()
            .done { movies in
                self.movies = movies
            }
            .cauterize()
    }
}


extension MoviesViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = self.movies?.count {
            return count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! MovieCell
        cell.configure(movie: self.movies?[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let movie = self.movies?[indexPath.row] {
            self.showDetails(movie: movie)
        }
    }
}
